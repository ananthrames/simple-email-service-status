#Importing Libraries

import pipes
import os

# Get send quota of SES

GetSES():
    awscommand="aws " "ses " " get-send-quota" " | " " grep " "SentLast24Hours" " > " "/home/user/ses/ses.txt"
    cutcommand="sh " " /home/user/ses/cut.sh"
    os.system(awscommand)
    os.system(cutcommand)
    f=open("/home/user/ses/ses1.txt", "r")
    contents =f.read()
    con="Sending Statistics:" + pipes.quote(contents)
    mail_subject=open("/home/user/ses/mail.txt", "w")
    mail_subject.write("Subject:SES Sent Mail Status \nHi  \n\nSES sent mail status\n\n----\n\n")
    mail_subject.close()
    mail_contents=open("/home/user/ses/mail.txt", "a")
    mail_contents.write(con)
    mail_contents.close()
    
# Send mail using sendmail

SendMail():
    sendmail_command="sendmail " "admin@example.com " "< " "/home/user/ses/mail.txt "
    os.system(sendmail_command)

GetSES()
SendMail()